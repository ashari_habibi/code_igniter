<?php if(!defined('BASEPATH')) exit('Keluar dari sistem');

class Library_Model extends CI_Model
{

public function __construct(){
	 parent::__construct();
}

	public function model_take($num,$offset,$res){

		$this->db->order_by('nama_file', 'ASC');

	if($res=='default'){
		$data = $this->db->get('tb_document',$num,$offset);
	}
	elseif($res=='pedoman'){
		$data = $this->db->get_where('tb_document',array('tipe'=> 'Peraturan & Pedoman'),$num,$offset);
	}
	elseif($res=='informasi'){
		$data = $this->db->get_where('tb_document',array('tipe'=> 'Informasi Sertifikasi'),$num,$offset);
	}
	elseif($res=='readmore'){
		$data = $this->db->get_where('tb_document',array('id'=> $num));
	}
	else{
		$this->db->where("nama_file like '%$res%'");
		$data = $this->db->get('tb_document',$num,$offset);
		
	}

	return $data->result();
	}
	
	public function model_delete(){
		
		$nama = $_GET['delete'];
		$id = $_GET['delete_id'];
		
	$success =	$this->db->query("delete from tb_document where id='$id' and nama_file='$nama'");
		
		if($success){
			return true;
		}
		else{
			return false;
		}
	}
 
	public function show_query($id){
		
		$query = $this->db->query("select nama_file from tb_document where id='$id'");
		
		foreach($query->result() as $row){
			
			return $row->nama_file;
		}
	}
	
	public function get_jml($res){
		if($res=='default'){
			$jml = $this->db->get('tb_document');
		}
		elseif($res=='pedoman'){
			
			$jml = $this->db->query("select * from tb_document where tipe = 'Peraturan & Pedoman'");
		}
		elseif($res=='informasi'){
			
			$jml = $this->db->query("select * from tb_document where tipe = 'Informasi Sertifikasi'");
		}
		else{
			
			$jml = $this->db->query("select * from tb_document where nama_file like '%$res%'");
		}
		
		return $jml->num_rows();
	}

}
?>