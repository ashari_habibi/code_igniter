<?php

class Upload_Model extends CI_Model {

public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				
				$this->load->database('default');
        }
		
public	function input_file($user)
		{
	if(!empty($_FILES['userfile']['size'])){
			$tgl			= 	date('Y-m-d');
			$pdf 			= 	".pdf";	
			$username		=	$user;
			$judul  		=	$_POST['judul'].$pdf;
			$description  	=	$_POST['description'];
			$iorp  			=	$_POST['tipe'];
			$fileName 		= 	$_FILES['userfile']['name'];
			$tmpName  		= 	$_FILES['userfile']['tmp_name'];
			$fileSize 		= 	$_FILES['userfile']['size'];
			$fileType 		= 	$_FILES['userfile']['type'];
			$allowedExts 	= 	array(	"pdf", "doc", "docx"); 										
			$allowedMimeTypes = array( 'application/msword','text/pdf','image/gif','image/jpeg','image/png');
			
		
					if(empty($_POST['judul']))
					{
						$newname = $fileName;
					}
					else
					{
						$newname = $judul;
					}			
						
						$uploads_dir 	= 	"./Uploads";
					
					$query1 = $this->db->query("SELECT * FROM tb_document where nama_file = '$newname'");
					$num   = $query1->num_rows();
						
					if($num > 0){
						return "falsename";
					}
					else{
					
					if ( 3000000 < $fileSize  ) 
					{
						die( 'Please provide a smaller file [E/1].' );
					}
					else
					{
						if(!get_magic_quotes_gpc())
						{
							$newname = addslashes($newname);
						}
						
					$data 	=	array(
									'nama_file'	=>$newname,
									'size'		=>$fileSize,
									'Type'		=>$fileType,
									'tgl_upload'=>$tgl,
									'username'	=>$username,
									'deskripsi'	=>$description,
									'tipe'		=>$iorp
									);
						
					$input	=	$this->db->insert('tb_document',$data);
					
				if($input){
					$success = move_uploaded_file($tmpName, "$uploads_dir/$newname");
					
					return "true";
				}
				else
				{
					return false;
				}
		}
		}

		}
		else
		{
			return false;
		}
		}
}
?>