<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
	parent::__construct();

	$this->load->library('library_library');
	}
	
	public function index($id=NULL){
		
		session_start();
		
		$jml = $this->library_library->do_jml('default');

			//pengaturan pagination
			 $config['base_url'] = base_url().'/index.php/Library/index';
			 $config['total_rows'] = $jml;
			 $config['per_page'] = '5';
			 $config['first_page'] = 'Awal';
			 $config['last_page'] = 'Akhir';
			 $config['next_page'] = '&laquo;';
			 $config['prev_page'] = '&raquo;';
			 

			//inisialisasi config
			 $this->pagination->initialize($config);

			//buat pagination
			 $data['halaman'] = $this->pagination->create_links();

			//tamplikan data
			 $data['query'] = $this->library_library->library_take($config['per_page'], $id,'default');

			 $data['level'] = $_SESSION['level'];
			 
			 $data['jumlah'] = $jml;
			 
			 $data['no'] = $id + 1;
			 
		if(isset($_GET['condition'])){
			
			if($_GET['condition']==true){
				echo "<script>alert(\"Your data has been deleted!\")</script>";
			}
			else{
				echo "<script>alert(\"Failed while deleting your data!\")</script>";
			}
		}
		
		$this->load->view('Templates/Header');
		$this->load->view('Library1', $data);
		$this->load->view('Templates/Footer');
	}
	
	public function Do_delete() {
		

		
		$success =	$this->library_library->library_delete();
		
		if($success==true){
			header('location:Http://192.168.0.31/codeigniter/index.php/Library?condition=true');
		}
		else{
			header('location:Http://192.168.0.31/codeigniter/index.php/Library?condition=false');
		}
		
	}
	
	public function show(){
		
		session_start();
		
			
		
			$id 	= $_GET['id'];
			
			$filename =	$this->library_library->Do_show($id);
			
			$path = './Uploads/'.$filename;

			$file = $path;
			


  header('Content-type: application/pdf');
  header('Content-Disposition: inline; filename="' . $filename . '"');
  header('Content-Transfer-Encoding: binary');
  header('Accept-Ranges: bytes');
  @readfile($file);
  
	}
	
	public function Do_search($id=NULL){
		
		session_start();
	if(isset($_POST['search'])){
		
		$search = $_POST['search'];
		$_SESSION['search'] = $search;
	}
	else{
			
		$search = $_SESSION['search'];
	}
	
		$jml = $this->library_library->do_jml($search);

			//pengaturan pagination
			 $config['base_url'] = base_url().'/index.php/Library/Do_search';
			 $config['total_rows'] = $jml;
			 $config['per_page'] = '5';
			 $config['first_page'] = 'Awal';
			 $config['last_page'] = 'Akhir';
			 $config['next_page'] = '&laquo;';
			 $config['prev_page'] = '&raquo;';
			 

			//inisialisasi config
			 $this->pagination->initialize($config);

			//buat pagination
			 $data['halaman'] = $this->pagination->create_links();

			//tamplikan data
			 $data['query'] = $this->library_library->library_take($config['per_page'], $id,$search);

			 $data['level'] = $_SESSION['level'];

			 $data['no'] = $id + 1;
			 
			 $data['jumlah'] = $jml;
			 
		$this->load->view('Templates/Header');
		$this->load->view('Library1', $data);
		$this->load->view('Templates/Footer');
		
	}
	
	public function pedoman($id=NULL){
		
		session_start();
		
		$jml = $this->library_library->do_jml('pedoman');

			//pengaturan pagination
			 $config['base_url'] = base_url().'/index.php/Library/pedoman';
			 $config['total_rows'] = $jml;
			 $config['per_page'] = '5';
			 $config['first_page'] = 'Awal';
			 $config['last_page'] = 'Akhir';
			 $config['next_page'] = '&laquo;';
			 $config['prev_page'] = '&raquo;';
			 

			//inisialisasi config
			 $this->pagination->initialize($config);

			//buat pagination
			 $data['halaman'] = $this->pagination->create_links();

			//tamplikan data
			 $data['query'] = $this->library_library->library_take($config['per_page'], $id,'pedoman');

			 $data['level'] = $_SESSION['level'];

			 $data['no'] = $id + 1;
			 
			$data['jumlah'] = $jml;
			 
		$this->load->view('Templates/Header');
		$this->load->view('Library1', $data);
		$this->load->view('Templates/Footer');
	}
	
		public function informasi($id=NULL){
		
		session_start();
		
		$jml = $this->library_library->do_jml('informasi');

			//pengaturan pagination
			 $config['base_url'] = base_url().'/index.php/Library/informasi';
			 $config['total_rows'] = $jml;
			 $config['per_page'] = '5';
			 $config['first_page'] = 'Awal';
			 $config['last_page'] = 'Akhir';
			 $config['next_page'] = '&laquo;';
			 $config['prev_page'] = '&raquo;';
			 

			//inisialisasi config
			 $this->pagination->initialize($config);

			//buat pagination
			 $data['halaman'] = $this->pagination->create_links();

			//tamplikan data
			 $data['query'] = $this->library_library->library_take($config['per_page'], $id,'informasi');

			 $data['level'] = $_SESSION['level'];
			 
			 $data['no'] = $id + 1;
			 
			 $data['jumlah'] = $jml;
			 
		$this->load->view('Templates/Header');
		$this->load->view('Library1', $data);
		$this->load->view('Templates/Footer');
	}
	
	public function read(){
		
		session_start();
		
		$read = $_GET['id'];
		
		$data['query'] = $this->library_library->library_take($read,'0','readmore');

		$data['level'] = $_SESSION['level'];
		
		$data['jumlah'] = 1;
		
		$data['no'] = 1;
		
		$data['halaman'] = "";
			 
		$this->load->view('Templates/Header');
		$this->load->view('Library1', $data);
		$this->load->view('Templates/Footer');
		
	}

}
