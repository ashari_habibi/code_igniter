<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
				session_start();
		
		if(empty($_SESSION['username']))
		{
			header('location:Http://192.168.0.31/codeigniter/index.php/login');
		}
		else
		{
		$this->load->view('Templates/Header');
		$this->load->view('Signup1');
		$this->load->view('Templates/Footer');
		}
	}
	
	public function Do_Signup()
	{
						session_start();
		
		if(empty($_SESSION['username']))
		{
			header('location:Http://192.168.0.31/codeigniter/index.php/login');
		}
		else
		{
		$this->load->library('Signup_Library');
		
		$success = $this->signup_library->do_do_signup();

				echo $success;
				
		$this->load->view('Templates/Header');
		$this->load->view('Signup1');
		$this->load->view('Templates/Footer');
		
			}
		}
	
}
