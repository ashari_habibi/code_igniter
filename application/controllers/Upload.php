<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		session_start();
		if(empty($_SESSION['username']))
		{
			header('location:Http://192.168.0.31/codeigniter/index.php/login');
		}
		else
		{		
		$this->load->view('Templates/Header');
		$this->load->view('Upload1');
		$this->load->view('Templates/Footer');
		}
	}
	public function Do_Upload()
	{
		session_start();

		if(empty($_SESSION['username']))
		{
			header('location:Http://192.168.0.31/codeigniter/index.php/login');
		}
		else
		{			
		$this->load->Library('Upload_Library');
		
		$Success	=	$this->upload_library->Do_do_upload($_SESSION['username']);
		}
		if($Success=="true"){
			echo "<script>alert(\"Upload Success!\")</script>";
		$this->load->view('Templates/Header');
		$this->load->view('Upload1');
		$this->load->view('Templates/Footer');
		}
		elseif($Success=='falsename'){
			echo "<script>alert(\"Upload Failed ! Title already exist!\")</script>";
		$this->load->view('Templates/Header');
		$this->load->view('Upload1');
		$this->load->view('Templates/Footer');
		}
		
		else{
			echo "<script>alert(\"Upload Failed!\")</script>";
		$this->load->view('Templates/Header');
		$this->load->view('Upload1');
		$this->load->view('Templates/Footer');
		}
		
	}
}
