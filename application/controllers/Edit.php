<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
	parent::__construct();

	$this->load->library('Edit_Library');
	}
	public function index()
	{
		
		$data['query'] = $this->edit_library->do_get_data();
		
		session_start();
		
		$this->load->view('Templates/Header');
		$this->load->view('Edit1', $data);
		$this->load->view('Templates/Footer');
	
	}
	
	public function Do_edit(){
		$success = $this->edit_library->do_do_edit();
		
		if($success==true){
			echo "<script>alert(\"File successfully edited\")</script>";
		}
		else{
			echo "<script>alert(\"File Failded to Edit File\")</script>";
		}
		
		header('location:Http://192.168.0.31/codeigniter/index.php/Library');
	}

}
