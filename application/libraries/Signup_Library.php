<?php 
 if( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Signup_Library {
	
	public function do_do_signup(){
		
		$CI = & get_instance();
		$CI->load->model('Signup_Model');
		
		$success = $CI->Signup_Model->input_user();
		
		
		if($success=="fail4"){
			return "<script>alert(\"Failed while inserting data!\")</script>";
		}
		elseif($success=="fail1"){
			
			return "<script>alert(\"Fill Your Username And Password!\")</script>";
		}
		elseif($success=="fail2"){
			
			return "<script>alert(\"Your Password Does Not Match!\")</script>";
		}
		elseif($success=="fail3"){
			return "<script>alert(\"Username already used!\")</script>";
		}
		elseif($success=="fail5"){
			return "<script>alert(\"Your Username or Password is too short!\")</script>";
		}
		else{
			return "<script>alert(\"Signup success username ".$success." successfuly added\")</script>";
			
		}
		
	}
}
	 ?>