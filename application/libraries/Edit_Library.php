<?php  if( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Edit_Library {

	public function do_get_data(){
		
				$CI = & get_instance();
		$CI->load->model('Edit_Model');
		
		$success = $CI->Edit_Model->get_data();
		
		return $success;
	}
	
	public function do_do_edit(){
		
		$CI = & get_instance();
		$CI->load->model('Edit_Model');
		
		$success = $CI->Edit_Model->get_edit();
		
		return $success;
	}
	
}
	?>