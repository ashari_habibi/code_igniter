<?php 
 if( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Library_Library {
	
	

	public function library_take($a,$b,$c){
		
		$CI = & get_instance();
		$CI->load->model('Library_Model');
		
		$success = $CI->Library_Model->model_take($a,$b,$c);
		
		return $success;
		
	}
	
	public function library_delete(){
		
		$CI = & get_instance();
		$CI->load->model('Library_Model');
		
		$success = $CI->Library_Model->model_delete();
		
		return $success;
	}
	
	public function Do_show($id){
		
				$CI = & get_instance();
		$CI->load->model('Library_Model');
		
		$success = $CI->Library_Model->show_query($id);
		
		return $success;
		
	}
	
	public function do_jml($res){
		
		$CI = & get_instance();
		$CI->load->model('Library_Model');
		
		return $CI->Library_Model->get_jml($res);
	}

}